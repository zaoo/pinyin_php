<?php
/**
 * PHP 汉字转拼音 [包含20902个基本汉字+5059生僻字]
 * @author 楼教主(cik520@qq.com)
 * @version v1.2
 * @note 请开启 mb_string 扩展
 *
 * -- 20170925 ligz --
 * + 分离 拼音映射到数据文件 pinyin.data
 * + 在英文之后跟随中文的情况时增加空格间隔
 * + 输出首字符的时候英文按词取首
 * 
 */
/* 测试用例 * /
$start_time = microtime(1);

var_dump(pinyin('对多音字无能为力'));
var_dump(pinyin('最全的PHP汉字转拼音库，比百度词典还全（dict.baidu.com）'));
var_dump(pinyin('试试：㐀㐁㐄㐅㐆㐌㐖㐜'));
var_dump(pinyin('一起开始数：12345'));
var_dump(pinyin('海南'));
var_dump(pinyin('乌鲁木齐'));
var_dump(pinyin('前总理朱镕基'));
var_dump(pinyin('仅首字母', 'first'));
var_dump(pinyin('占-位-符-为-空', 'all', ''));
var_dump(pinyin('不允许中文以外的字符', 'first', '', ''));
var_dump(pinyin('不允许Abc de中文bddc以外c的字符', 'first', '','/[A-Za-z0-9]/'));
var_dump(pinyin('前总理朱镕基'));

for ($i=0; $i<1e4; $i++) { // 性能次数，转换1万次
    pinyin('对多音字无能为力');
    pinyin('最全的PHP汉字转拼音库，比百度词典还全（dict.baidu.com）');
    pinyin('试试：㐀㐁㐄㐅㐆㐌㐖㐜');
    pinyin('一起开始数：12345');
    pinyin('海南');
    pinyin('乌鲁木齐');
    pinyin('前总理朱镕基');
    pinyin('仅首字母', 'first');
    pinyin('占-位-符-为-空', 'all', '');
    pinyin('不允许中文以外的字符', 'first', '', '');
}

echo number_format(microtime(1) - $start_time, 6);
echo "\n END \n";
//*/

/**
 * 中文转拼音 (utf8版,gbk转utf8也可用)
 * @param string $str         utf8字符串
 * @param string $ret_format  返回格式 [all:全拼音|first:首字母|one:仅第一字符首字母]
 * @param string $placeholder 无法识别的字符占位符
 * @param string $allow_chars 允许的非中文字符
 * @return string             拼音字符串
 */
function pinyin($str, $ret_format = 'all', $placeholder = '_', $allow_chars = '/[a-zA-Z\d ]/') {
    static $pinyins = null;

    if (null === $pinyins) {
        $data = file_get_contents(__DIR__."/pinyin.data");
        $rows = explode("\n", $data);

        $pinyins = array();
        foreach($rows as $v) {
            list($py, $vals) = explode(':', $v);
            $chars = explode(',', $vals);

            foreach ($chars as $char) {
                $pinyins[$char] = $py;
            }
        }
    }

    $str = trim($str);
    $len = mb_strlen($str, 'UTF-8');
    $rs = '';
    $is_return_first=$ret_format=='first';
    $space=' ';
    $pre_is_space=false;
    $pre_is_char=false;
    for ($i = 0; $i < $len; $i++) {
        $chr = mb_substr($str, $i, 1, 'UTF-8');
        $asc = ord($chr);
        $out=$chr;
        $out_type="char";
        if ($asc < 0x80) { // 0-127
            if (preg_match($allow_chars, $chr)) { // 用参数控制正则
                //$pre_is_char=true;
                // if($is_return_first){
                //     if(!$pre_is_char){
                //         $rs .= $chr; // 0-9 a-z A-Z 空格
                //     }// 上一个字为字符时忽略
                // }else{
                //     $rs .= $chr; // 0-9 a-z A-Z 空格
                // }
            } else { // 其他字符用填充符代替
                $out_type="placeholder";
                // if($is_return_first){// 其他字符不参与连接
                //     continue;
                // }
                // $out=$placeholder;
                // $pre_is_char=false;
                //$rs .= $placeholder;
            }
        } else { // 128-255
            if (isset($pinyins[$chr])) {
                $out=$pinyins[$chr];
                $out_type="zh_char";
                //$pre_is_char=false;
                // if($pre_is_char){
                //     $rs.=' ';
                // }
                // $rs .= 'first' === $ret_format ? $pinyins[$chr][0] : ($pinyins[$chr] . ' ');
            } else {
                $out_type="placeholder";
                // $rs .= $placeholder;
                // $pre_is_char=true;
            }
        }
        switch($out_type){
            case "char":
                if($is_return_first&&$pre_is_char){
                    continue;// 当输出首字母的时候非第一个字符忽略
                }else{
                    $rs.=$out;
                }
                $pre_is_char=true;
                $pre_is_space=false;
            break;
            case "zh_char":
                if($is_return_first){
                    $rs.=$out[0];
                }else{
                    if(!$pre_is_space){
                        $rs.=$space;
                    }
                    $rs.=$out.$space;
                    $pre_is_space=true;
                }
                $pre_is_char=false;
            break;
            case "placeholder":
            default:
                $pre_is_space=false;
                $pre_is_char=false;
                if($is_return_first){// 忽略无效的字符
                    continue;
                }else{
                    $rs.=$placeholder;
                }
            break;
        }

        if ('one' === $ret_format && '' !== trim($rs)) {
            return $rs[0];
        }
    }

    return trim($rs, $space);
}
